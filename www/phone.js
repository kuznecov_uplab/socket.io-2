var cb = throttle(10, handleOrientation)

window.addEventListener("deviceorientation", cb, true);

function throttle(delay, fn, immediate) {
    "use strict";

    var now = Date.now() - (immediate ? delay : 0);

    return function () {
        var from = Date.now();
        var interval = from - now;
        if (interval < delay) {
            return;
        }

        now = from;
        fn.apply(this, arguments);
    };
}

function handleOrientation(event) {
  socket.emit('ev', {
    alpha: event.alpha,
    beta: event.beta,
    gamma: event.gamma
})
}