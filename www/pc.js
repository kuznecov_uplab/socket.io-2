var compassDisc = document.getElementById("compassDiscImg");
var msg = document.getElementById("m");

function deviceOrientationHandler(tiltLR, tiltFB, dir) {

    if (about(90, dir)) {
        msg.innerHTML = 'direction 1'
    }
    else if (about(113, dir)) {
        msg.innerHTML = 'direction 2'
    }
    else if (about(10, dir)) {
        msg.innerHTML = 'direction 3'
    }
    else {
        msg.innerHTML = '&nbsp;'
    }

    compassDisc.style.transform = "rotateZ(" + dir + "deg) rotateY(" + tiltFB + "deg) rotateX(" + tiltLR + "deg)";
}

function about(i, v) {
    return i - 5 <= v && i + 5 >= v;
}

socket.on('error', function (reason) {
    console.error('Unable to connect Socket.IO', reason);
});
socket.on('connect', function () {
    console.info('Connected');
});

socket.on("ev", function (eventData) {
    console.log(eventData);

    // gamma: Tilting the device from left to right. Tilting the device to the right will result in a positive value.
    // gamma: Het kantelen van links naar rechts in graden. Naar rechts kantelen zal een positieve waarde geven.
    var tiltLR = eventData.gamma;

    // beta: Tilting the device from the front to the back. Tilting the device to the front will result in a positive value.
    // beta: Het kantelen van voor naar achteren in graden. Naar voren kantelen zal een positieve waarde geven.
    var tiltFB = eventData.beta;

    // alpha: The direction the compass of the device aims to in degrees.
    // alpha: De richting waarin de kompas van het apparaat heen wijst in graden.
    var dir = eventData.alpha;

    // Call the function to use the data on the page.
    // Roep de functie op om de data op de pagina te gebruiken.
    deviceOrientationHandler(tiltLR, tiltFB, dir);
});