const bodyParser = require('body-parser');
const express = require('express');

const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(express.static('www'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/index', function(req, res){
  res.sendfile('index.html');
});

io.on('connection', function(socket){
  socket.on('ev', function(ev){
        socket.broadcast.emit('ev', ev);
    });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});





var os = require('os');

var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address);
        }
    }
}

console.log(addresses);